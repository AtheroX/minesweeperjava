package Game;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;

import Core.Board;
import Core.BoardSprite;
import Core.Sprite;
import Core.Window;

/**
 * Test Petita demostraci� de representacions possibles. s'avan�a donant
 * enter a la consola
 *
 */

public class TestNewBoard {

	public static void main(String[] args) {
		Board b = new Board();
		Window f = new Window(b);
		f.setSize(800, 800);

		Scanner sc = new Scanner(System.in);

		// cas 2: Buscaminas

		System.out.println("cas 2: Text - Buscamines");

		b.setColorbackground(0xb1adad);
		b.setActborder(true);
		String[] lletres = { "", "1", "2", "3", "4", "5", "6", "7", "8", "*" }; // qu�
																				// s'ha
																				// d'escriure
																				// en
																				// cada
																				// casella
																				// en
																				// base
																				// al
																				// nombre
		b.setText(lletres);
		int[] colorlletres = { 0x0000FF, 0x00FF00, 0xFFFF00, 0xFF0000, 0xFF00FF, 0x00FFFF, 0x521b98, 0xFFFFFF, 0xFF8000,
				0x7F00FF };
		b.setColortext(colorlletres);
		String[] etiquetes2 = { "Mines: 10", "Temps: 600" };
		f.setLabels(etiquetes2);
		f.setActLabels(true);
		f.setTitle("Cercamines");

		int[][] matriu2 = { { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }, { 9, 9, 1, 1, 2, 9, 9, 5, 9, 9 },
				{ 9, 9, 9, 9, 3, 9, 9, 9, 9, 9 }, { 9, 9, 5, 9, 6, 9, 9, 9, 9, 9 }, { 9, 9, 9, 9, 6, 9, 5, 9, 9, 9 },
				{ 9, 9, 8, 9, 6, 9, 9, 9, 6, 9 }, { 9, 9, 5, 9, 6, 9, 9, 9, 9, 9 }, { 1, 1, 1, 9, 9, 9, 9, 9, 9, 9 },
				{ 0, 0, 1, 2, 9, 9, 9, 9, 9, 9 }, { 0, 0, 0, 1, 9, 4, 1, 7, 8, 9 }, };

		b.draw(matriu2, 't');
		sc.nextLine();

	}

}